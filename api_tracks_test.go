package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_handlerApi_malformedURL(t *testing.T) {
	//instantiate a mock http server(server only for testing)
	//Register our handlerStudent <-- actual logic
	ts := httptest.NewServer(http.HandlerFunc(handlerAPI))
	defer ts.Close()

	testCases := []string{
		ts.URL + "/igcinfo/ap",
		ts.URL + "/igcinfo/apsi/igc",
	}
	for _, tstring := range testCases {
		resp, err := http.Get(tstring)
		if err != nil {
			t.Errorf("Error making the GET request, %s", err)
		}

		if resp.StatusCode != http.StatusNotFound {
			t.Errorf("for route %s, expected statuscode %d, received %d", tstring, http.StatusNotFound, resp.StatusCode)
			return
		}
	}
}

// GET /Student/
// Empty array back
func Test_handlerApi_getAllIDs_Empty(t *testing.T) {
	//instantiate a mock http server(server only for testing)

	ts := httptest.NewServer(http.HandlerFunc(handlerAPI))
	defer ts.Close()

	resp, err := http.Get(ts.URL + "/igcinfo/api/igc")
	if err != nil {
		t.Errorf("Error making the get request, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected StatusCode %d, received %d", http.StatusOK, resp.StatusCode)
		return
	}

	var a []int
	err = json.NewDecoder(resp.Body).Decode(&a)
	fmt.Println(a)
	if err != nil {
		t.Errorf("Error parsing the expected JSON body. Got error %s", err)
	}

	if len(a) != 0 {
		t.Errorf("Expected empty array. Got %d", a)
	}

}

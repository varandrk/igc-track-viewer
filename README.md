# README #

This README would normally document whatever steps are necessary to get your application up and running.


An online service that will allow users to browse information about IGC files. IGC is an international file format for soaring track files that are used by paragliders and gliders.

# USAGE # 
Visit https://igc-track-viewer2.herokuapp.com/

For server information: `/igcinfo/api`

You need to post a URL to a legit IGC file in JSON format to
 `https://igc-track-viewer2.herokuapp.com/igcinfo/api/igc`: 
```
{
"url": "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc"
}
```
You will then get an ID for the track in return. 

If you want to view all track ID's: 

Visit: `/igcinfo/api/igc`

If you want to see info about a specific track: 
Visit: `/igcinfo/api/igc/<track id>`

If you want to see information about a specific field:
```
/igcinfo/api/<track id>/Pilot        --- The pilot
/igcinfo/api/<track id>/H_Date       --- Header date
/igcinfo/api/<track id>/Glider       --- Glider type
/igcinfo/api/<track id>/Glider_id    --- Glider id
/igcinfo/api/<track id>/Track_length --- Length of the track
```

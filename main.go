package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"
)

type ApiInfo struct {
	Uptime  string `json:"Uptime"`
	Info    string `json:"Info"`
	Version string `json:"Version"`
}

func replyWithAPIinfo(w *http.ResponseWriter) {
	api.Uptime = timeFormat(serverStart)
	json.NewEncoder(*w).Encode(api)
}

//ISO8601 formatted uptime
func timeFormat(startTime time.Time) string {
	endTime := time.Now()
	return fmt.Sprintf("P%dY%dM%dDT%dH%dM%dS",
		endTime.Year()-startTime.Year(), endTime.Month()-startTime.Month(), endTime.Day()-startTime.Day(),
		endTime.Hour()-startTime.Hour(), endTime.Minute()-startTime.Minute(), endTime.Second()-startTime.Second())
}

//--------------------------------------------------------------------------------
var serverStart time.Time
var api = ApiInfo{Uptime: timeFormat(serverStart), Info: "Service for IGC tracks.", Version: "v1"}
var db TrackDB = TrackDB{}
var IDs []int

//--------------------------------------------------------------------------------

func init() {
	serverStart = time.Now()
}

func main() {
	db.Init()
	var p string
	if port := os.Getenv("PORT"); port != "" {
		p = ":" + port
	} else {
		p = ":8080"
	}
	http.HandleFunc("/igcinfo/", handlerAPI)
	http.ListenAndServe(p, nil)

}

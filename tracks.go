package main

import (
	"math/rand"
	"reflect"
	"time"
)

type Track struct {
	H_Date       time.Time `json:"Date"`
	Pilot        string    `json:"Pilot"`
	Glider       string    `json:"Glider Type"`
	Glider_id    string    `json:"Glider ID"`
	Track_length float64   `json:"TrackLength"`
}

type TrackDB struct {
	tracks map[int]Track
}

func (db *TrackDB) Init() {
	db.tracks = make(map[int]Track)
}

func (db *TrackDB) Get(id int) (Track, bool) {
	t, ok := db.tracks[id]
	return t, ok
}

func (db *TrackDB) getField(field string, id int) (string, bool) {
	exists := true
	r := reflect.ValueOf(db.tracks[id])
	f := reflect.Indirect(r).FieldByName(field)
	// If field does not exist in struct
	if f.String() == "<invalid Value>" {
		exists = false
	}
	return string(f.String()), exists
}

func (db *TrackDB) Add(t Track) int {
	//Create unique ID to track
	id := rand.Int()

	//Check if track exists
	_, ok := db.Get(id)

	//If exists, create new ID
	for ok {
		id = rand.Int()
		_, ok = db.Get(id)
	}

	db.tracks[id] = t

	return id

}

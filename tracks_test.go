package main

import (
	"fmt"
	"testing"
	"time"
)

func Test_addTrack(t *testing.T) {
	db := TrackDB{}
	db.Init()
	track := Track{time.Now(), "Miguel Angel Gordillo", "RV8", "EC-XLL", 425.95571656352956}
	id := db.Add(track)
	if len(db.tracks) != 1 {
		t.Error("Wrong track count")
	}

	getTrack, _ := db.Get(id)
	if getTrack.Pilot != "Miguel Angel Gordillo" {
		t.Error("Track was not added")
	}

}

func Test_multipleTracks(t *testing.T) {

	db := TrackDB{}
	db.Init()
	// Create ID's for tracks to manually add to the db
	IDs := [3]int{4037200794235010051, 4037200794235010052, 4037200794235010053}

	// Create tracks
	testData := map[int]Track{
		IDs[0]: {time.Now(), "Miguel Angel Gordillo", "RV8", "EC-XLL", 425.95571656352956},
		IDs[1]: {time.Now(), "Varand Rebni Kjer", "RV9", "NC-XUU", 425.95571656352956},
		IDs[2]: {time.Now(), "Jorgen Berntsen", "RV8", "TC-XKK", 425.95571656352956},
	}

	i := 0
	// Add tracks to db
	for _, track := range testData {
		// Add tracks
		db.tracks[IDs[i]] = track
		i += 1
	}

	// Check that every track is added to db
	if len(db.tracks) != len(testData) {
		t.Error("Wrong number of tracks")
	}
	// Check every track form testData against db
	for key := range db.tracks {

		track, _ := db.Get(key)
		trackTest, _ := testData[key]

		if track.H_Date != trackTest.H_Date {
			t.Error("Wrong Header date")
		}

		if track.Pilot != trackTest.Pilot {
			t.Error("Wrong Pilot")
		}

		if track.Glider != trackTest.Glider {
			t.Error("Wrong glider type")
		}

		if track.Glider_id != trackTest.Glider_id {
			t.Error("Wrong glider id")
		}

		if track.Track_length != trackTest.Track_length {
			t.Error("Wrong track length")
		}

	}

}

func Test_getField(t *testing.T) {
	//Test for getting a field from a track (THIS FUNCTION ONLY WORKS ON STRINGS)
	db := TrackDB{}
	db.Init()
	//Create a track
	track := Track{time.Now(), "Miguel Angel Gordillo", "RV8", "EC-XLL", 425.95571656352956}
	//Add it to the db and get id
	id := db.Add(track)

	// Get the fields of type string
	fields := map[string]string{"Pilot": "Pilot", "Glider": "Glider", "Glider_id": "Glider_id", "H_Date": "H_Date", "Track_length": "Track_length"}

	// Correct field data
	data := map[string]string{"Pilot": "Miguel Angel Gordillo", "Glider": "RV8", "Glider_id": "EC-XLL", "H_Date": "<time.Time Value>", "Track_length": "<float64 Value>"}

	//Loop through the fields
	for key := range fields {

		returnedField, ok := db.getField(key, id)

		if !ok {
			t.Error("The field does not exist in Track struct")
		}
		// Check if returned field is the same as track
		if returnedField != data[key] {
			fmt.Println(returnedField)
			t.Error("Can't get correct field")
		}
	}

}
